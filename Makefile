default: docker-exec-in-service-instance
dist-tarball: docker-exec-in-service-instance.tar.gz

.PHONY: default dist-bin

SRCS := $(shell find -name '*.go')
docker-exec-in-service-instance: $(SRCS)
	go build -o $@

LICENSE.Apache-2.0:
	wget -O $@ https://www.apache.org/licenses/LICENSE-2.0.txt

docker-exec-in-service-instance.tar.gz: docker-exec-in-service-instance NOTICE LICENSE.GPLv3 LICENSE.Apache-2.0
	tar --create --gzip --file $@ $^

clean:
	rm docker-exec-in-service-instance LICENSE.Apache-2.0

.PHONY: clean
