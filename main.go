/*
 * Copyright (C) 2019 Ratchanan Srirattanamet.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. [1]
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * [1] Note that this software can be considered to be a derivative work
 * of Docker client SDK, licensed under Apache License, Version 2.0. If
 * you choose to use the later version of GPL, you must make sure that
 * such version is compatible with Apache License 2.0.
 */

package main

import (
	"context"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
)

func main() {
	if len(os.Args) < 3 {
		panic("Not enough argument given")
	}

	serviceName := os.Args[1]
	command := os.Args[2:]

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}
	cli.NegotiateAPIVersion(ctx)

	info, err := cli.Info(ctx)
	if err != nil {
		panic(err)
	}
	myNodeID := info.Swarm.NodeID

	if len(myNodeID) == 0 {
		panic("This node is not the part of swarm")
	}

	taskList, err := cli.TaskList(ctx, types.TaskListOptions{
		Filters: filters.NewArgs(
			filters.Arg("service", serviceName),
		),
	})
	if err != nil {
		panic(err)
	}

	var task swarm.Task
	found := false

	for _, t := range taskList {
		if t.Status.State == "running" && t.NodeID == myNodeID {
			found = true
			task = t
		}
	}

	if !found {
		panic("Running instance not found on this node")
	}

	containerID := task.Status.ContainerStatus.ContainerID

	execID, err := cli.ContainerExecCreate(ctx, containerID, types.ExecConfig{
		Cmd:          command,
		AttachStdout: true,
		AttachStderr: true,
	})
	if err != nil {
		panic(err)
	}

	res, err := cli.ContainerExecAttach(ctx, execID.ID, types.ExecStartCheck{
		Detach: false,
		Tty:    false,
	})
	if err != nil {
		panic(err)
	}

	defer res.Close()
	stdcopy.StdCopy(os.Stdout, os.Stderr, res.Reader)

	execInfo, err := cli.ContainerExecInspect(ctx, execID.ID)
	if err != nil {
		panic(err)
	}

	os.Exit(execInfo.ExitCode)
}
